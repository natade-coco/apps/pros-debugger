import * as Sentry from '@sentry/gatsby'

class Log {
  static debug = (message: string) => {
    console.log(message)
  }

  static error = (message: string) => {
    console.error(message)
    Sentry.captureEvent({ message })
  }
}

export default Log
