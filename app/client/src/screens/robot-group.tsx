import React, { useCallback, useEffect } from 'react'
import { RouteComponentProps, Link } from "@reach/router"
import { useSelector, useDispatch } from 'react-redux'

import { Device, RobotGroup } from '../entities/pros'
import { AppDispatch } from '../app/store'
import { RootState } from '../app/rootReducer'
import Screen from '../components/screen'
import Title from '../components/title'
import { robotSelectors, getRobots } from '../slices/robot'
import { deviceSelectors, getDevices } from '../slices/device'
import { getRobotGroups, robotGroupSelectors } from '../slices/robot-group'

interface Props extends RouteComponentProps {
  deviceId?: string,
  robotGroupId?: string
}

const RobotGroupScreen = (props: Props) => {
  const dispatch: AppDispatch = useDispatch()
  const { deviceId, robotGroupId } = props

  const device = useSelector<RootState, Device>(state => deviceSelectors.selectById(state, deviceId))
  const robotGroup = useSelector<RootState, RobotGroup>(state => robotGroupSelectors.selectById(state, robotGroupId))
  const robots = useSelector(robotSelectors.selectAll)

  useEffect(() => {
    dispatchGetRobots()
  }, [])

  useEffect(() => {
    if (device) {
      if (!robotGroup) {
        dispatch(getRobotGroups(device.id))
      }
    } else {
      dispatch(getDevices())
    }
  }, [device, robotGroup])

  const dispatchGetRobots = useCallback(() => {
    dispatch(getRobots({ deviceId, robotGroupId }))
  }, [deviceId, robotGroupId])

  return (
    <Screen breadCrumbProps={{ device, robotGroup }}>
      <section className="section">
        <Title
          title="Robots"
          onSyncPressed={dispatchGetRobots}
        />
        <div className="container my-4">
          {robots.map(robot => (
            <Link key={robot.id} className="box" to={`robots/${robot.id}`}>
              <p className="mb-1 has-text-weight-bold">{robot.name || '(No name)'}</p>
              <p className="is-size-7">ID: {robot.id}</p>
              <p className="is-size-7">Shop: {robot.shopName}</p>
            </Link>
          ))}
        </div>
      </section>
    </Screen>
  )
}

export default RobotGroupScreen
