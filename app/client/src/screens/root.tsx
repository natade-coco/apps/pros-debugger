import React, { useEffect } from 'react'
import { RouteComponentProps, useNavigate } from "@reach/router"

interface Props extends RouteComponentProps {
}

const IndexScreen = (props: Props) => {
  const navigate = useNavigate()

  useEffect(() => {
    navigate('/devices')
  }, [])

  return <div />
}

export default IndexScreen
