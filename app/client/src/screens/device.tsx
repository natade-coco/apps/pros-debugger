import React, { useCallback, useEffect } from 'react'
import { RouteComponentProps, Link } from "@reach/router"
import { useSelector, useDispatch } from 'react-redux'

import { Device } from '../entities/pros'
import { AppDispatch } from '../app/store'
import { RootState } from '../app/rootReducer'
import Screen from '../components/screen'
import Title from '../components/title'
import { deviceSelectors, getDevices } from '../slices/device'
import { robotGroupSelectors, getRobotGroups } from '../slices/robot-group'

interface Props extends RouteComponentProps {
  deviceId?: string
}

const DeviceScreen = (props: Props) => {
  const dispatch: AppDispatch = useDispatch()
  const { deviceId } = props

  const device = useSelector<RootState, Device>(state => deviceSelectors.selectById(state, deviceId))
  const robotGroups = useSelector(robotGroupSelectors.selectAll)

  useEffect(() => {
    dispatchGetRobotGroups()
  }, [])

  useEffect(() => {
    if (!device) {
      dispatch(getDevices())
    }
  }, [device])

  const dispatchGetRobotGroups = useCallback(() => {
    dispatch(getRobotGroups(deviceId))
  }, [deviceId])

  return (
    <Screen breadCrumbProps={{ device }}>
      <section className="section">
        <Title
          title="Robot Groups"
          onSyncPressed={dispatchGetRobotGroups}
        />
        <div className="container my-4">
          {robotGroups.map(robotGroup => (
            <Link key={robotGroup.id} className="box" to={`robot_groups/${robotGroup.id}`}>
              <p className="mb-1 has-text-weight-bold">{robotGroup.name || '(No name)'}</p>
              <p className="is-size-7">ID: {robotGroup.id}</p>
              <p className="is-size-7">Shop: {robotGroup.shopName}</p>
            </Link>
          ))}
        </div>
      </section>
    </Screen>
  )
}

export default DeviceScreen
