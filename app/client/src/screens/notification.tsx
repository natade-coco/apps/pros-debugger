import React, { useMemo, useEffect, useCallback } from 'react'
import { RouteComponentProps } from "@reach/router"
import { useSelector, useDispatch } from 'react-redux'
import { Flipper, Flipped } from 'react-flip-toolkit'

import { Device, Robot, RobotGroup } from '../entities/pros'
import { AppDispatch } from '../app/store'
import { RootState } from '../app/rootReducer'
import Screen from '../components/screen'
import Title from '../components/title'
import { getDevices, deviceSelectors } from '../slices/device'
import { getRobotGroups, robotGroupSelectors } from '../slices/robot-group'
import { getRobots, robotSelectors } from '../slices/robot'
import { clearNotifications, notificationSelectors } from '../slices/notification'

interface Props extends RouteComponentProps {
  deviceId?: string,
  robotGroupId?: string,
  robotId?: string
}

const NotificationScreen = (props: Props) => {
  const dispatch: AppDispatch = useDispatch()
  const { deviceId, robotGroupId, robotId } = props

  const device = useSelector<RootState, Device>(state => deviceSelectors.selectById(state, deviceId))
  const robotGroup = useSelector<RootState, RobotGroup>(state => robotGroupSelectors.selectById(state, robotGroupId))
  const robot = useSelector<RootState, Robot>(state => robotSelectors.selectById(state, robotId))
  const notifications = useSelector(notificationSelectors.selectAll)

  useEffect(() => {
    if (device) {
      if (robotGroup) {
        if (!robot) {
          dispatch(getRobots({ deviceId: device.id, robotGroupId: robotGroup.id }))
        }
      } else {
        dispatch(getRobotGroups(device.id))
      }
    } else {
      dispatch(getDevices())
    }
  }, [device, robotGroup, robot])

  const notificationsForDisplay = useMemo(() => (
    notifications
      .filter(notification => (notification.robotId === robotId))
      .reverse()
  ), [notifications])

  const onClearPressed = useCallback(() => {
    dispatch(clearNotifications())
  }, [])

  return (
    <Screen breadCrumbProps={{ device, robotGroup, robot }}>
      <section className="section">
        <Title
          title="Notifications"
          extraButton={(
            <button className="button is-small" onClick={onClearPressed}>
              <span className="icon is-small has-text-danger">
                <i className="fas fa-trash-alt"></i>
              </span>
            </button>
          )}
        />
        <div className="container my-4">
          <Flipper flipKey={notificationsForDisplay.map(n => n.id).join('')}>
            {(notificationsForDisplay.length > 0) ? (
              notificationsForDisplay.map(notification => (
                <Flipped key={notification.id} flipId={notification.id}>
                  <div className="box">
                    <p className="has-text-weight-bold">{notification.type}</p>
                    <p className="is-size-7"><i>{JSON.stringify(notification.data)}</i></p>
                    <div className="level is-mobile">
                      <div className="level-left">
                        <p className="is-size-7 has-text-grey">{notification.id}</p>
                      </div>
                      <div className="level-right">
                        <p className="is-size-7 has-text-grey">{new Date(notification.timestamp).toISOString()}</p>
                      </div>
                    </div>
                  </div>
                </Flipped>
              ))
            ) : (
              <p className="is-size-7 has-text-centered has-text-grey my-5">Notifications will be displayed here</p>
            )}
          </Flipper>
        </div>
      </section>
    </Screen>
  )
}

export default NotificationScreen
