import React, { useCallback, useEffect, useState } from 'react'
import { RouteComponentProps, useNavigate } from "@reach/router"
import { useSelector, useDispatch } from 'react-redux'

import { Destination, Device, Robot, RobotGroup } from '../entities/pros'
import { AppDispatch } from '../app/store'
import { RootState } from '../app/rootReducer'
import Screen from '../components/screen'
import Title from '../components/title'
import { getDevices, deviceSelectors } from '../slices/device'
import { getRobotGroups, robotGroupSelectors } from '../slices/robot-group'
import { getRobots, getStatus, robotSelectors } from '../slices/robot'
import { call, callCancel, destinationSelectors, getDestinations } from '../slices/destination'
import { enableNotification, onNotificationReceived, isNotificationEnabledSelector } from '../slices/notification'

interface Props extends RouteComponentProps {
  deviceId?: string,
  robotGroupId?: string,
  robotId?: string
}

const RobotScreen = (props: Props) => {
  const dispatch: AppDispatch = useDispatch()
  const navigate = useNavigate()
  const { deviceId, robotGroupId, robotId } = props

  const device = useSelector<RootState, Device>(state => deviceSelectors.selectById(state, deviceId))
  const robotGroup = useSelector<RootState, RobotGroup>(state => robotGroupSelectors.selectById(state, robotGroupId))
  const robot = useSelector<RootState, Robot>(state => robotSelectors.selectById(state, robotId))
  const destinations = useSelector(destinationSelectors.selectAll)
  const isNotificationEnabled = useSelector(isNotificationEnabledSelector)

  const [showsNotificationModal, setShowsNotificationModal] = useState(false)

  useEffect(() => {
    if (device) {
      if (robotGroup) {
        if (!robot) {
          dispatch(getRobots({ deviceId: device.id, robotGroupId: robotGroup.id }))
        }
      } else {
        dispatch(getRobotGroups(device.id))
      }
    } else {
      dispatch(getDevices())
    }
  }, [device, robotGroup, robot])

  useEffect(() => {
    if (robot && !robot.status) {
      dispatchGetStatus()
      dispatchGetDestinations()
    }
  }, [robot])

  const dispatchGetDestinations = useCallback(() => {
    dispatch(getDestinations({ deviceId, robotId }))
  }, [deviceId, robotId])

  const dispatchCall = useCallback((destination: Destination) => {
    dispatch(call({ deviceId, robotId, destination}))
  }, [deviceId, robotId])

  const dispatchCallCancel = useCallback((destination: Destination) => {
    dispatch(callCancel({ deviceId, robotId, destination}))
  }, [deviceId, robotId])

  const dispatchGetStatus = useCallback(() => {
    dispatch(getStatus({ deviceId, robotId }))
  }, [deviceId, robotId])

  const onMonitorPressed = useCallback(() => {
    if (isNotificationEnabled) {
      navigate('notifications')
    } else {
      setShowsNotificationModal(true)
    }
  }, [isNotificationEnabled])

  const onNotificationModalOKPressed = useCallback(() => {
    dispatch(enableNotification(notification=> {
      dispatch(onNotificationReceived(notification))
    })).then(() => {
      setShowsNotificationModal(false)
      navigate('notifications')
    })
  }, [])

  const onNotificationModalCancelPressed = useCallback(() => {
    setShowsNotificationModal(false)
  }, [])

  return (
    <Screen breadCrumbProps={{ device, robotGroup, robot }}>
      <section className="section">
        <Title
          title="Status"
          onSyncPressed={dispatchGetStatus}
          extraButton={(
            <button className="button is-small" onClick={onMonitorPressed}>
              <span className="icon is-small">
                <i className="fas fa-eye"></i>
              </span>
            </button>
          )}
        />
        <div className="container my-4">
          {robot?.status &&
            <div className="box">
              <p className="mb-1 has-text-weight-bold">{robot.status.nearestDestination?.name}</p>
              <p className="is-size-7">
                Charge stage: <i>{robot.status.chargeStage}</i> {' | '}
                Move state: <i>{robot.status.moveState}</i> {' | '}
                Robot state: <i>{robot.status.robotState}</i> {' | '}
                Robot pose: <i>({robot.status.robotPosition.x.toFixed(3)}, {robot.status.robotPosition.y.toFixed(3)}, {robot.status.robotPosition.angle.toFixed(3)})</i> {' | '}
                Robot power: <i>{robot.status.robotPower}</i>
              </p>
            </div>
          }
        </div>
      </section>
      <section className="section">
        <Title
          title="Destinations"
          onSyncPressed={dispatchGetDestinations}
        />
        <div className="container my-4">
          {destinations.map(destination => (
            <div key={destination.id} className="box">
              <div className="columns is-mobile is-vcentered">
                <div className="column">
                  <p className="mb-1 has-text-weight-bold">{destination.name}</p>
                  <p className="is-size-7">Type: {destination.type}</p>
                </div>
                <div className="column is-narrow">
                  <CallButtons
                    onCancelPressed={() => dispatchCallCancel(destination)}
                    onCallPressed={() => dispatchCall(destination)}
                  />
                </div>
              </div>
            </div>
          ))}
        </div>
        <p className="is-size-7 has-text-centered has-text-grey my-5">(最大200件まで表示)</p>
      </section>
      <NotificationModal
        isActive={showsNotificationModal}
        onOKPressed={onNotificationModalOKPressed}
        onCancelPressed={onNotificationModalCancelPressed}
      />
    </Screen>
  )
}

type CallButtonsProps = {
  onCancelPressed: () => void,
  onCallPressed: () => void
}

const CallButtons = (props: CallButtonsProps) => {
  return (
    <div className="buttons">
      <button className="button" onClick={props.onCancelPressed}>
        <span className="icon is-small">
          <i className="fas fa-stop" />
        </span>
      </button>
      <button className="button" onClick={props.onCallPressed}>
        <span className="icon is-small">
          <i className="fas fa-play" />
        </span>
      </button>
    </div>
  )
}

type NotificationModalProps = {
  isActive: boolean,
  onOKPressed: () => void,
  onCancelPressed: () => void
}

const NotificationModal = (props: NotificationModalProps) => {
  const [isLoading, setIsLoading] = useState(false)

  const onOKPressed = useCallback(() => {
    setIsLoading(true)
    props.onOKPressed()
  }, [])

  return (
    <div className={`modal ${props.isActive && 'is-active'}`}>
      <div className="modal-background"></div>
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">{'Warning'}</p>
        </header>
        <section className="modal-card-body">
          Are you sure you want to enable the notification feature? If you enable it, notifications in all other apps using the same device will be disabled.
        </section>
        <footer className="modal-card-foot has-text-centered">
          <button className="button is-rounded" onClick={props.onCancelPressed}>
            Cancel
          </button>
          <button className={`button is-rounded is-primary ${isLoading && 'is-loading'}`} onClick={onOKPressed}>
            OK
          </button>
        </footer>
      </div>
    </div>
  )
}

export default RobotScreen
