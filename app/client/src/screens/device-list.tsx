import React, { useCallback, useEffect } from 'react'
import { RouteComponentProps, Link } from "@reach/router"
import { useSelector, useDispatch } from 'react-redux'

import { AppDispatch } from '../app/store'
import { deviceSelectors, getDevices } from '../slices/device'
import Screen from '../components/screen'
import Title from '../components/title'

interface Props extends RouteComponentProps {
}

const DeviceListScreen = (props: Props) => {
  const dispatch: AppDispatch = useDispatch()

  const devices = useSelector(deviceSelectors.selectAll)

  useEffect(() => {
    dispatchGetDevices()
  }, [])

  const dispatchGetDevices = useCallback(() => {
    dispatch(getDevices())
  }, [])

  return (
    <Screen>
      <section className="section">
        <Title
          title="Devices"
          onSyncPressed={dispatchGetDevices}
        />
        <div className="container my-4">
          {devices.map(device => (
            <Link key={device.id} className="box" to={device.id}>
              <p className="mb-1 has-text-weight-bold">{device.name}</p>
              <p className="is-size-7">ID: {device.id}</p>
              <p className="is-size-7">Region: {device.region}</p>
            </Link>
          ))}
        </div>
      </section>
    </Screen>
  )
}

export default DeviceListScreen
