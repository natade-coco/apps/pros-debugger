interface User {
  id: string | null
}

interface App {
  id: string | null
}

export interface Session {
  token: string | null
  user: User | null
  app: App | null
  env?: string
}
