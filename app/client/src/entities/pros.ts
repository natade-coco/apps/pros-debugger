export interface Device {
  id: string,
  name: string,
  region: string
}

export interface RobotGroup {
  id: string,
  name: string,
  shopName: string
}

export interface Robot {
  id: string,
  name: string,
  shopName: string,
  status?: Status
}

export interface Destination {
  id: string,
  name: string,
  type: string
}

export interface Position {
  x: number,
  y: number,
  angle: number
}

export interface Status {
  chargeStage: string,
  moveState: string,
  robotState: string,
  robotPosition: Position,
  robotPower: number,
  nearestDestination?: Destination
}

export interface Notification {
  id: string,
  type: string,
  robotId: string,
  robotGroupId: string,
  deviceId: string,
  timestamp: number,
  data: any
}
