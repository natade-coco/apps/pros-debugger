import React, { ReactNode } from 'react'
import { useSelector } from 'react-redux'
import ClipLoader from 'react-spinners/ClipLoader'

import Navbar from './navbar'
import Breadcrumb, { Props as BreadcrumbProps } from './breadcrumb'
import { errorSelector, isLoadingSelector } from '../slices/app'

type Props = {
  children?: ReactNode,
  breadCrumbProps?: BreadcrumbProps
}

const Screen = (props: Props) => {
  return (
    <div>
      <Navbar />
      <Breadcrumb {...props.breadCrumbProps} />
      {props.children}
      <Loading />
      <ErrorMessage />
    </div>
  )
}

const ErrorMessage = () => {
  const error = useSelector(errorSelector)
  const isLoading = useSelector(isLoadingSelector)
  if (!error || isLoading) return <></>

  return (
    <div className="container px-4">
      <article className="message is-danger">
        <div className="message-body">
          {error}
        </div>
      </article>
    </div>
  )
}

const Loading = () => {
  const isLoading = useSelector(isLoadingSelector)
  if (!isLoading) return <></>

  return (
    <div className="has-text-centered my-5">
      <ClipLoader size={32} color="darkgray" />
    </div>
  )
}

export default Screen
