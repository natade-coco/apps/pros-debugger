import React, { ReactNode } from 'react'

type Props = {
  title: string,
  onSyncPressed?: () => void,
  extraButton?: ReactNode
}

const Title = (props: Props) => {
  return (
    <div className="container">
      <div className="level is-mobile">
        <div className="level-left">
          <h5 className="title is-5">{props.title}</h5>
        </div>
        <div className="level-right">
          <div className="buttons">
            {props.extraButton}
            {props.onSyncPressed &&
              <button className="button is-small" onClick={props.onSyncPressed}>
                <span className="icon is-small">
                  <i className="fas fa-sync"></i>
                </span>
              </button>
            }
          </div>
        </div>
      </div>
    </div>
  )
}

export default Title
