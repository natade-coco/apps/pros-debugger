import * as React from 'react'
import { Helmet } from 'react-helmet'
import { StaticQuery, graphql } from 'gatsby'

interface DefaultLayoutProps extends React.HTMLProps<HTMLDivElement> {
  children: any
}

class DefaultLayout extends React.PureComponent<DefaultLayoutProps, {}> {
  public render() {
    return (
      <StaticQuery
        query={graphql`
          query LayoutQuery {
            site {
              siteMetadata {
                title
              }
            }
          }
        `}
        render={(data) => (
          <>
            <Helmet
              htmlAttributes={
                {
                  class: "has-navbar-fixed-top"
                }
              }
              titleTemplate={`%s | ${data.site.siteMetadata.title}`}
              defaultTitle={data.site.siteMetadata.title}
              meta={[
                {
                  name: 'viewport',
                  content:
                    'width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no',
                },
                {
                  httpEquiv: 'Content-Security-Policy',
                  content: "default-src 'self'; script-src https://use.fontawesome.com;"
                }
              ]}
              script={[
                {
                  src: 'https://use.fontawesome.com/releases/v5.3.1/js/all.js',
                  defer: true,
                },
              ]}
              link={[
                {
                  href: 'https://fonts.googleapis.com/css?family=M+PLUS+1p',
                  rel: 'stylesheet',
                },
              ]}
            />
            {this.props.children}
          </>
        )}
      />
    )
  }
}

export default DefaultLayout
