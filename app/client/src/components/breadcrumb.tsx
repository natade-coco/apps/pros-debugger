import React, { memo, useMemo } from 'react'
import { Link } from '@reach/router'

import { Device, Robot, RobotGroup } from '../entities/pros'

export type Props = {
  device?: Device,
  robotGroup?: RobotGroup,
  robot?: Robot
}

interface BreadLink {
  name: string,
  path: string
}

const Breadcrumb = memo((props: Props) => {
  const { device, robotGroup, robot } = props

  const links: BreadLink[] = useMemo(() => {
    let ls: BreadLink[] = [{ name: 'Top', path: '/' }]
    if (device) {
      ls.push({ name: device.name || '(No name)', path: `/devices/${device.id}` })
      if (robotGroup) {
        ls.push({ name: robotGroup.name || '(No name)', path: `/devices/${device.id}/robot_groups/${robotGroup.id}` })
        if (robot) {
          ls.push({ name: robot.name || '(No name)', path: `/devices/${device.id}/robot_groups/${robotGroup.id}/robots/${robot.id}` })
        }
      }
    }
    return ls
  }, [device, robotGroup, robot])

  return (
    <nav className="breadcrumb has-succeeds-separator m-0 p-4" aria-label="breadcrumbs">
      <ul>
        {links.map(({ name, path }, index) => (
          <li key={index}><Link to={path}>{name}</Link></li>
        ))}
      </ul>
    </nav>
  )
})

export default Breadcrumb
