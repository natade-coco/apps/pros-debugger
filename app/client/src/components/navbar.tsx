import React from 'react'
import { Link } from '@reach/router'

import logo from '../images/bellabot-face.png'

const Navbar = () => (
  <nav className="navbar is-fixed-top is-primary" role="navigation" aria-label="main navigation">
    <div className="navbar-brand">
      <Link className="navbar-item" to="/">
        <img src={logo} />
        <span className="ml-3 has-text-weight-bold">PROS Debugger</span>
      </Link>
    </div>
  </nav>
)

export default Navbar
