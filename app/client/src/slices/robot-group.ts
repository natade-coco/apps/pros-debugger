import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit'

import { RobotGroup } from '../entities/pros'
import { RootState } from '../app/rootReducer'
import { getDevices } from './device'
import ProsService from '../services/server/pros'

const prosService = new ProsService()

// Entity Adapter

const entityAdapter = createEntityAdapter<RobotGroup>()

// Selectors

const robotGroupISelector = (state: RootState) => state.robotGroup

export const robotGroupSelectors = entityAdapter.getSelectors(robotGroupISelector)

// Async Thunks

export const getRobotGroups = createAsyncThunk('robotGroup/list', async (deviceId: string): Promise<RobotGroup[]> => {
  try {
    const robotGroups = await prosService.getRobotGroups(deviceId)
    return robotGroups
  } catch (err) {
    throw err
  }
})

// Slice

const slice = createSlice({
  name: 'robotGroup',
  initialState: entityAdapter.getInitialState(),
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getRobotGroups.fulfilled, entityAdapter.setAll)
      .addCase(getDevices.fulfilled, entityAdapter.removeAll)
  }
})

export default slice.reducer
