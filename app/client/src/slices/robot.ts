import { createAsyncThunk, createEntityAdapter, createSlice, PayloadAction } from '@reduxjs/toolkit'

import { Robot, Status } from '../entities/pros'
import { RootState } from '../app/rootReducer'
import { getDevices } from './device'
import { getRobotGroups } from './robot-group'
import ProsService from '../services/server/pros'

const prosService = new ProsService()

// Entity Adapter

const entityAdapter = createEntityAdapter<Robot>()

// Selectors

const robotISelector = (state: RootState) => state.robot

export const robotSelectors = entityAdapter.getSelectors(robotISelector)

// Async Thunks

export interface GetRobotsRequest {
  deviceId: string,
  robotGroupId: string
}

export const getRobots = createAsyncThunk('robot/list', async (request: GetRobotsRequest): Promise<Robot[]> => {
  try {
    const robots = await prosService.getRobots(request.deviceId, request.robotGroupId)
    return robots
  } catch (err) {
    throw err
  }
})

export interface GetStatusRequest {
  deviceId: string,
  robotId: string
}

export interface GetStatusResponse {
  robotId: string,
  status: Status
}

export const getStatus = createAsyncThunk('robot/getStatus', async (request: GetStatusRequest): Promise<GetStatusResponse> => {
  try {
    const status = await prosService.getStatus(request.deviceId, request.robotId)
    return { robotId: request.robotId, status }
  } catch (err) {
    throw err
  }
})

// Slice

const slice = createSlice({
  name: 'robot',
  initialState: entityAdapter.getInitialState(),
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getRobots.fulfilled, entityAdapter.setAll)
      .addCase(getStatus.fulfilled, (state, { payload }: PayloadAction<GetStatusResponse>) => {
        state.entities[payload.robotId].status = payload.status
      })
      .addCase(getRobotGroups.fulfilled, entityAdapter.removeAll)
  }
})

export default slice.reducer
