import {
  createSlice,
  PayloadAction,
  createSelector,
  createAsyncThunk,
  isPending,
  isRejected,
  isFulfilled,
} from '@reduxjs/toolkit'
import { RootState } from '../app/rootReducer'
import { Session } from '../entities/app'
import PocketService from '../services/pocket'
import AppService from '../services/server/app'

const appService = new AppService()
const pocket = new PocketService()

export interface State {
  session: Session
  pendingActions: string[]
  error: string | null
}

const initialState: State = {
  session: null,
  pendingActions: [],
  error: null
}

// Selectors

const appISelector = (state: RootState) => state.app

export const isLoadingSelector = createSelector(
  appISelector,
  (state: State) => state.pendingActions.length > 0
)

export const errorSelector = createSelector(
  appISelector,
  (state: State) => state.error
)

// Thunks

export const appStart = createAsyncThunk(
  'app/start',
  async (): Promise<Session> => {
    try {
      const jwt = await pocket.requestSignJWT()
      const session = await appService.getSession(jwt)
      return session
    } catch (err) {
      throw err
    }
  }
)

// Slice

const getOriginalActionType = (action: PayloadAction<any>): string =>
  action.type.slice(0, action.type.lastIndexOf('/'))

const startLoading = (state: State, action: PayloadAction<any>) => {
  const set = new Set(state.pendingActions)
  set.add(getOriginalActionType(action))
  state.pendingActions = Array.from(set)
}

const loadingSuccess = (state: State, action: PayloadAction<any>) => {
  const set = new Set(state.pendingActions)
  set.delete(getOriginalActionType(action))
  state.pendingActions = Array.from(set)
  state.error = null
}

const loadingFailed = (
  state: State,
  action: PayloadAction<any, string, any, any>
) => {
  const set = new Set(state.pendingActions)
  set.delete(getOriginalActionType(action))
  state.pendingActions = Array.from(set)
  state.error = action.error?.message
}

const slice = createSlice({
  name: 'app',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(
        appStart.fulfilled,
        (state, { payload }: PayloadAction<Session>) => {
          state.session = payload
        }
      )
      .addMatcher(isPending, startLoading)
      .addMatcher(isFulfilled, loadingSuccess)
      .addMatcher(isRejected, loadingFailed)
  },
})

export default slice.reducer
