import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit'

import { Destination } from '../entities/pros'
import { RootState } from '../app/rootReducer'
import { getRobots } from './robot'
import ProsService from '../services/server/pros'

const prosService = new ProsService()

// Entity Adapter

const entityAdapter = createEntityAdapter<Destination>()

// Selectors

const destinationISelector = (state: RootState) => state.destination

export const destinationSelectors = entityAdapter.getSelectors(destinationISelector)

// Async Thunks

export interface GetDestinationsRequest {
  deviceId: string,
  robotId: string
}

export const getDestinations = createAsyncThunk('destination/list', async (request: GetDestinationsRequest): Promise<Destination[]> => {
  try {
    const destinations = await prosService.getDestinations(request.deviceId, request.robotId)
    return destinations
  } catch (err) {
    throw err
  }
})

export interface CallRequest {
  deviceId: string,
  robotId: string,
  destination: Destination
}

export const call = createAsyncThunk('destination/call', async (request: CallRequest): Promise<void> => {
  try {
    await prosService.call(request.deviceId, request.robotId, request.destination)
  } catch (err) {
    throw err
  }
})

export const callCancel = createAsyncThunk('destination/callCancel', async (request: CallRequest): Promise<void> => {
  try {
    await prosService.callCancel(request.deviceId, request.robotId, request.destination)
  } catch (err) {
    throw err
  }
})

// Slice

const slice = createSlice({
  name: 'destination',
  initialState: entityAdapter.getInitialState(),
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getDestinations.fulfilled, entityAdapter.setAll)
      .addCase(getRobots.fulfilled, entityAdapter.removeAll)
  }
})

export default slice.reducer
