import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit'

import { RootState } from '../app/rootReducer'
import { Device } from '../entities/pros'
import ProsService from '../services/server/pros'

const prosService = new ProsService()

// Entity Adapter

const entityAdapter = createEntityAdapter<Device>()

// Selectors

const deviceISelector = (state: RootState) => state.device

export const deviceSelectors = entityAdapter.getSelectors(deviceISelector)

// Async Thunks

export const getDevices = createAsyncThunk('device/list', async (): Promise<Device[]> => {
  try {
    const devices = await prosService.getDevices()
    return devices
  } catch (err) {
    throw err
  }
})

// Slice

const slice = createSlice({
  name: 'device',
  initialState: entityAdapter.getInitialState(),
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getDevices.fulfilled, entityAdapter.setAll)
  }
})

export default slice.reducer
