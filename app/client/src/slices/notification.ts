import { createAsyncThunk, createEntityAdapter, createSlice, createSelector, EntityState } from '@reduxjs/toolkit'

import { RootState } from '../app/rootReducer'
import { Notification } from '../entities/pros'
import ProsService from '../services/server/pros'

const prosService = new ProsService()

// Entity Adapter

interface NotificationEntityState extends EntityState<Notification> {
  isNotificationEnabled: boolean
}

const entityAdapter = createEntityAdapter<Notification>()

// Selectors

const notificationISelector = (state: RootState) => state.notification

export const notificationSelectors = entityAdapter.getSelectors(notificationISelector)

export const isNotificationEnabledSelector = createSelector(
  notificationISelector,
  (state: NotificationEntityState) => state.isNotificationEnabled
)

// Async Thunks

export const enableNotification = createAsyncThunk('notifications/enable', async (listener: (notification: Notification) => void): Promise<void> => {
  return prosService.enableNotification(listener)
})

export const onNotificationReceived = createAsyncThunk('notifications/onReceived', async (notification: Notification): Promise<Notification> => {
  return notification
})

export const clearNotifications = createAsyncThunk('notifications/clear', async (): Promise<void> => {
  return
})

// Slice

const initialState: NotificationEntityState = entityAdapter.getInitialState({
  isNotificationEnabled: false
})

const slice = createSlice({
  name: 'notification',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(enableNotification.fulfilled, state => {
        state.isNotificationEnabled = true
      })
      .addCase(onNotificationReceived.fulfilled, entityAdapter.addOne)
      .addCase(clearNotifications.fulfilled, entityAdapter.removeAll)
  }
})

export default slice.reducer
