import React, { useEffect, useState } from 'react'
import ClipLoader from 'react-spinners/ClipLoader'
import { useDispatch } from 'react-redux'

import './styles.scss'
import { appStart } from '../slices/app'
import { AppDispatch } from '../app/store'
import Layout from '../components/Layout'
import Router from '../components/Router'
import Root from '../screens/root'
import DeviceList from '../screens/device-list'
import Device from '../screens/device'
import RobotGroup from '../screens/robot-group'
import Robot from '../screens/robot'
import Notification from '../screens/notification'

const Page = () => {
  const dispatch: AppDispatch = useDispatch()
  const [isAppReady, setIsAppReady] = useState(false)

  useEffect(() => {
    dispatch(appStart()).then(() => {
      setIsAppReady(true)
    })
  }, [])

  return (
    <>
      {isAppReady ? (
        <Layout>
          <Router>
            <Root path="/" />
            <DeviceList path="/devices" />
            <Device path="/devices/:deviceId" />
            <RobotGroup path="/devices/:deviceId/robot_groups/:robotGroupId" />
            <Robot path="/devices/:deviceId/robot_groups/:robotGroupId/robots/:robotId" />
            <Notification path="/devices/:deviceId/robot_groups/:robotGroupId/robots/:robotId/notifications" />
          </Router>
        </Layout>
      ) : (
        <Loader />
      )}
    </>
  )
}

const Loader = () => (
  <div className="hero is-fullheight">
    <div className="hero-body">
      <div className="container" style={{ flex: '0 0 auto' }}>
        <ClipLoader size={50} color="darkgray" />
      </div>
    </div>
  </div>
)

export default Page
