import { combineReducers } from '@reduxjs/toolkit'

import AppReducer from '../slices/app'
import DeviceReducer from '../slices/device'
import RobotGroupReducer from '../slices/robot-group'
import RobotReducer from '../slices/robot'
import DestinationReducer from '../slices/destination'
import NotificationReducer from '../slices/notification'

const rootReducer = combineReducers({
  app: AppReducer,
  device: DeviceReducer,
  robotGroup: RobotGroupReducer,
  robot: RobotReducer,
  destination: DestinationReducer,
  notification: NotificationReducer
})

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer
