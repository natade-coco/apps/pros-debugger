import React from 'react'
import { Provider } from 'react-redux'
import store from './store'

export default ({ element }: { element: JSX.Element | JSX.Element[] }) => {
  return <Provider store={store}>{element}</Provider>
}
