import Pocket from '@natade-coco/pocket-sdk'

export default class PocketService {
  getProfile = (scope: string[]) => {
    if (process.env.NODE_ENV === 'development') {
      return Promise.resolve({
        name: 'ナタデココ',
        fullName: 'ナタデココ タロウ',
        email: 'natadecoco@natade-coco.com',
        tel: '012-3456-9780',
        image: 'https://bulma.io/images/placeholders/128x128.png',
      })
    }
    return Pocket.getProfile(scope)
  }

  getEthereumAddress = () => {
    if (process.env.NODE_ENV === 'development') {
      return Promise.resolve('0x1a2b3c')
    }
    return Pocket.getEthereumAddress()
  }

  requestSignJWT = () => {
    if (process.env.NODE_ENV === 'development') {
      return Promise.resolve(process.env.GATSBY_APP_TOKEN)
    }
    return Pocket.requestSignJWT()
  }

  connectToPayoffApp = async (
    target: string,
    path: string,
    allows: string[]
  ) => {
    if (process.env.NODE_ENV === 'development') {
      return 'https://natade-coco.com'
    }
    const url = await Pocket.connectToPayoffApp(target, path, allows)
    return url
  }
}
