import { io, protocol } from 'socket.io-client'
import { feathers } from '@feathersjs/feathers'
import socketio from '@feathersjs/socketio-client'

const NewFeathersApp = () => {
  if (typeof window === 'undefined' || !window.document) {
    return null
  }
  const transports =
    process.env.NODE_ENV === 'development'
      ? ['websocket']
      : ['polling', 'websocket']
  const socket = io(process.env.GATSBY_API_URL || window.location.origin, {
    transports,
    timeout: 10000,
  })
  socket.io
    .on('reconnect', () => {
      // コンテンツ復帰時サブスクリプションが切れる不具合の対処
      window.location.reload()
    })
    .on('error', (err) => {
      console.log('onerror', JSON.stringify(err))
      socket.connect()
    })
  socket
    .on('connect', () => {
      console.log('connected')
    })
    .on('disconnect', (reason) => {
      console.log(reason)
      socket.connect()
    })
    .on('connect_error', (reason) => {
      console.log(reason)
      socket.connect()
    })
  const app = feathers()
  app.configure(socketio(socket))

  return app
}

export const serverApp = NewFeathersApp()
