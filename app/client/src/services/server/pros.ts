import { Application, FeathersService, Id, Service } from '@feathersjs/feathers/lib'
import { serverApp } from './index'

import { Device, RobotGroup, Robot, Destination, Status, Notification } from '../../entities/pros'
import Log from '../../utils/log'

export default class ProsService {
  feathers: {
    devices: FeathersService<Application, Service<Device>>,
    robotGroups: FeathersService<Application, Service<RobotGroup>>,
    robots: FeathersService<Application, Service<Robot>>,
    destinations: FeathersService<Application, Service<Destination>>,
    status: FeathersService<Application, Service<Status>>,
    notifications: FeathersService<Application, Service<Notification>>
  }

  constructor() {
    this.feathers = {
      devices: serverApp?.service('devices'),
      robotGroups: serverApp?.service('robot-groups'),
      robots: serverApp?.service('robots'),
      destinations: serverApp?.service('destinations'),
      status: serverApp?.service('status'),
      notifications: serverApp?.service('notifications')
    }
  }

  getDevices = async (): Promise<Device[]> => {
    try {
      const result = await this.feathers.devices.find()
      Log.debug(`[getDevices] result=${JSON.stringify(result)}`)
      if (result instanceof Array) {
        return result
      } else {
        return [result]
      }
    } catch (err) {
      throw err
    }
  }

  getRobotGroups = async (deviceId: string): Promise<RobotGroup[]> => {
    try {
      const result = await this.feathers.robotGroups.find({
        query: { device: deviceId }
      })
      Log.debug(`[getRobotGroups] result=${JSON.stringify(result)}`)
      return (result instanceof Array) ? result : [result]
    } catch (err) {
      throw err
    }
  }

  getRobots = async (deviceId: string, robotGroupId: string): Promise<Robot[]> => {
    try {
      const result = await this.feathers.robots.find({
        query: { device: deviceId, group_id: robotGroupId }
      })
      Log.debug(`[getRobots] result=${JSON.stringify(result)}`)
      return (result instanceof Array) ? result : [result]
    } catch (err) {
      throw err
    }
  }

  getDestinations = async (deviceId: string, robotId: string): Promise<Destination[]> => {
    try {
      const result = await this.feathers.destinations.find({
        query: { device: deviceId, robot_id: robotId }
      })
      Log.debug(`[getDestinations] result=${JSON.stringify(result)}`)
      return (result instanceof Array) ? result : [result]
    } catch (err) {
      throw err
    }
  }

  call = async (deviceId: string, robotId: string, destination: Destination): Promise<void> => {
    try {
      await this.feathers.destinations.update(destination.id, destination, {
        query: { device: deviceId, robot_id: robotId }
      })
    } catch (err) {
      throw err
    }
  }

  callCancel = async (deviceId: string, robotId: string, destination: Destination): Promise<void> => {
    try {
      await this.feathers.destinations.update(destination.id, destination, {
        query: { device: deviceId, robot_id: robotId, cancel: true }
      })
    } catch (err) {
      throw err
    }
  }

  getStatus = async (deviceId: string, robotId: string): Promise<Status> => {
    try {
      const result = await this.feathers.status.find({
        query: { device_id: deviceId, robot_id: robotId, with_nearest_destination: true }
      })
      Log.debug(`[getStatus] result=${JSON.stringify(result)}`)
      return (result instanceof Array) ? result[0] : result
    } catch (err) {
      throw err
    }
  }

  enableNotification = async (listener: (notification: Notification) => void): Promise<void> => {
    try {
      await this.feathers.notifications.find({
        query: { enable: true }
      })
      this.feathers.notifications.on('created', listener)
    } catch (err) {
      throw err
    }
  }
}
