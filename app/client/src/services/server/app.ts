import { Session } from '../../entities/app'
import { serverApp } from './index'

export default class AppService {
  getSession = (jwt?: string): Promise<Session> => {
    return serverApp
      .service('authentication')
      .create({ strategy: 'natadecoco', token: jwt })
  }
}
