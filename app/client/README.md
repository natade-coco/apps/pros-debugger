# Client App

## Develop

#### 1. 開発用サーバーアプリを立ち上げる

#### 2. `client` フォルダ直下に `.env.development` ファイルを作成し、以下の環境変数を設定する

```
GATSBY_API_URL=<開発用サーバーアプリのエンドポイント>(必須)
GATSBY_APP_TOKEN=<Pocket(モバイルアプリ)の認証JWT>
SENTRY_DSN_URL=<SentryプロジェクトのDSN>
```

#### 3. `$ npm install && npm run start` => `localhost:8000` へアクセス
