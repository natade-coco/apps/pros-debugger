#!/bin/bash

# Usage: bump-version.sh [version]
#
# Arguments:
#   version       npm version  ex) 1.2.3, patch, minor

if [ $# -ne 1 ]; then
  echo "Please input version"
  exit 1
fi

cd `dirname $0`

# Client
cd client
npm version $1
git add package.json package-lock.json
cd ..

# Server
cd server
VERSION=`npm version $1`
git add package.json package-lock.json
cd ..

git commit -m "bump version to $VERSION"
