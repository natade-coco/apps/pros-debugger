import app from '../../src/app';

describe('\'robots\' service', () => {
  it('registered the service', () => {
    const service = app.service('robots');
    expect(service).toBeTruthy();
  });
});
