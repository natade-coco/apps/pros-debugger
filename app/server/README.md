# Server App

## 開発手順

一部の環境変数を取得するために、一度管理コンソールからアプリを登録しておく必要があります。

### 1. 環境変数ファイルを作成

`server` フォルダ直下に `.env` ファイルを作成し、以下の環境変数を設定します。

| 変数名 | 説明 | デフォルト | 備考 |
| :-- | :-- | :-- | :-- |
| NATADECOCO_ID | natadeCOCO ID | | 管理コンソールのアプリ詳細から取得 |
| NATADECOCO_SECRET | natadeCOCO Secret | | 管理コンソールのアプリ詳細から取得 |
| NODE_ENV | stg または prd | prd | |
| EXEC_ENV | local または global | global | localの場合、ユーザー認証をスキップ |
| SERVER_SENTRY_DSN_URL | SentryプロジェクトのDSN | | Sentryでログ収集する場合のみ設定 |
| PROS_URL | PROSのURL | http://pros.default.svc.cluster.local:9050 | |
| DEVICE_NAME | デバイス名 | natadeCOCO | |
| DEVICE_ID | デバイスID | | 起動時にデバイス登録を行う場合に設定 |
| DEVICE_SECRET | デバイスシークレット | | 起動時にデバイス登録を行う場合に設定 |
| DEVICE_REGION | デバイスのリージョン | ap-northeast-1 | |
| NOTIFICATION_URL | PROSからの通知を受けるURL | http://{APP_ID}.{APP_ID}-{DEVICE_ID}.svc.cluster.local:3030/notifications | `{APP_ID}` (アプリID) および `{DEVICE_ID}` (配信ユニットID) は変数として指定可能 |

#### `.env` ファイルの例

```
NATADECOCO_ID=XXXXXXX
NATADECOCO_SECRET=XXXXXXX
NODE_ENV=stg
EXEC_ENV=local
PROS_URL=http://localhost:9050
DEVICE_NAME=natadeCOCO
DEVICE_ID=XXXXXXX
DEVICE_SECRET=XXXXXXX
DEVICE_REGION=ap-northeast-1
```

### 2. 初期設定(インストール)

```
npm install
```

### 3. サーバーアプリを立ち上げる

```
npm start
```

`localhost:3030` に立ち上がります。
