import { Params } from "@feathersjs/feathers";
import { AuthenticationBaseStrategy, AuthenticationResult } from "@feathersjs/authentication";
import { SDK as Hub, ServiceType } from "@natade-coco/hub-sdk";

export class NatadeCOCOStrategy extends AuthenticationBaseStrategy {
  get configuration() {
    const authConfig = this.authentication?.configuration;
    const config = super.configuration || {};

    return {
      service: authConfig.service,
      entity: authConfig.entity,
      entityId: authConfig.entityId,
      errorMessage: "Invalid token",
      tokenField: config.tokenField,
      ...config,
    };
  }

  async verifyJWT(jwt: string) {
    const id = this.app?.get("natadecoco_id");
    const secret = this.app?.get("natadecoco_secret");
    const client = await Hub.init({
      id: id,
      secret: secret,
      test: this.app?.get("node_env") === "stg",
    });
    return client.VerifyJWT(jwt, ServiceType.AppHubService);
  }

  async authenticate(data: AuthenticationResult, params: Params) {
    const { tokenField } = this.configuration;
    const token = data[tokenField];
    const verifyResult =
      this.app?.get("exec_env") === "global"
        ? await this.verifyJWT(token)
        : { issuer: "did:ethr:0xfdd75875ab1bb0748e1e6cd20e4b3dd50f7fb7da" };
    return {
      authentication: { strategy: this.name },
      app: {
        id: this.app?.get("natadecoco_id"),
      },
      user: {
        id: verifyResult.issuer,
      },
    };
  }
}
