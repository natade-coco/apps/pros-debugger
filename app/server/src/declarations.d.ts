import { AuthenticationService } from "@feathersjs/authentication/lib";
import { Application as ExpressFeathers } from "@feathersjs/express";
import { Devices } from "./services/devices/devices.class";
import { RobotGroups } from "./services/robot-groups/robot-groups.class";
import { Robots } from "./services/robots/robots.class";
import { Destinations } from "./services/destinations/destinations.class";
import { Status } from "./services/status/status.class";
import { Maps } from "./services/maps/maps.class";
import { Notifications } from "./services/notifications/notifications.class";

// A mapping of service names to types. Will be extended in service files.
export interface ServiceTypes {
  "/authentication": AuthenticationService;
  "/devices": Devices;
  "/robot-groups": RobotGroups;
  "/robots": Robots;
  "/destinations": Destinations;
  "/status": Status;
  "/maps": Maps;
  "/notifications": Notifications;
}
// The application instance type that will be used everywhere else
export type Application = ExpressFeathers<ServiceTypes>;
