import compress from "compression";
import helmet from "helmet";
import cors from "cors";
import history from "connect-history-api-fallback";

import { feathers } from "@feathersjs/feathers";
import configuration from "@feathersjs/configuration";
import * as express from "@feathersjs/express";
import socketio from "@feathersjs/socketio";

import { Application } from "./declarations";
import logger from "./logger";
import middleware from "./middleware";
import services from "./services";
import appHooks from "./app.hooks";
import channels from "./channels";
import authentication from "./authentication";
// Don't remove this comment. It's needed to format import lines nicely.
import dotenv from "dotenv";

const app: Application = express.default(feathers());

// Load app configuration
app.configure(configuration());

// Load from .env
dotenv.config();
app.set("natadecoco_id", process.env.NATADECOCO_ID);
app.set("natadecoco_secret", process.env.NATADECOCO_SECRET);
app.set("node_env", process.env.NODE_ENV || "prd");
app.set("exec_env", process.env.EXEC_ENV || "global");
app.set("pros_url", process.env.PROS_URL || "http://pros.default.svc.cluster.local:9050");
app.set("device_name", process.env.DEVICE_NAME || "natadeCOCO")
app.set("device_id", process.env.DEVICE_ID)
app.set("device_secret", process.env.DEVICE_SECRET)
app.set("device_region", process.env.DEVICE_REGION || "ap-northeast-1")
app.set("notification_url", process.env.NOTIFICATION_URL)

// Enable security, CORS, compression, favicon and body parsing
app.use(helmet({ contentSecurityPolicy: false }));
app.use(cors());
app.use(compress());
app.use(history());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// Host the public folder
app.use("/", express.static(app.get("public")));

// Set up Plugins and providers
app.configure(express.rest());
app.configure(socketio({ allowEIO3: true }));

// Configure other middleware (see `middleware/index.ts`)
app.configure(middleware);
app.configure(authentication);
// Set up our services (see `services/index.ts`)
app.configure(services);
// Set up event channels (see channels.ts)
app.configure(channels);

// Configure a middleware for 404s and the error handler
app.use(express.notFound());
app.use(express.errorHandler({ logger } as any));

app.hooks(appHooks);

export default app;
