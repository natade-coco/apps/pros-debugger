// Initializes the `destination` service on path `/destination`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Destinations } from './destinations.class';
import hooks from './destinations.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'destinations': Destinations & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/destinations', new Destinations(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('destinations');

  service.hooks(hooks);
}
