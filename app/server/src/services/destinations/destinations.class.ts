import { Id, NullableId, Params, ServiceMethods } from '@feathersjs/feathers'
import Axios, { AxiosInstance } from 'axios'

import { Application } from '../../declarations'

export interface Data {
  id: string,
  name: string,
  type: string
}

const mapDestination = (destination: any): Data => ({
  id: `${destination.name}/${destination.type}`,
  name: destination.name,
  type: destination.type
})

interface ServiceOptions {}

export class Destinations implements ServiceMethods<Data> {
  app: Application
  options: ServiceOptions
  axios: AxiosInstance

  constructor (options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
    this.axios = this.createAxios()
  }

  createAxios = () => {
    const axios = Axios.create({
      baseURL: this.app.get('pros_url') + '/api'
    })
    axios.interceptors.response.use(response => {
      if (response.data.code !== 0) {
        throw new Error(response.data.msg)
      }
      return response
    })
    return axios
  }

  async setup () {}

  async find (params?: Params): Promise<Data[]> {
    const deviceId = params?.query?.device
    const robotId = params?.query?.robot_id

    try {
      const response = await this.axios.get('/destinations', {
        params: {
          device: deviceId,
          robot_id: robotId,
          page_size: 200,
          page_index: 1
        }
      })
      const responseData = response.data
      const destinations: Data[] = responseData.data.destinations.map((destination: any) => mapDestination(destination))
      return destinations
    } catch (err) {
      throw err
    }
  }

  async get (id: Id, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async create (data: Data, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async update (id: NullableId, data: Data, params?: Params): Promise<Data> {
    const deviceId = params?.query?.device
    const robotId = params?.query?.robot_id
    const cancel = params?.query?.cancel

    try {
      const path = cancel ? '/robot/cancel/call' : '/robot/call'
      await this.axios.post(path, {
        deviceId,
        robotId,
        destination: {
          name: data.name,
          type: data.type
        }
      })
    } catch (err) {
      throw err
    }

    return data;
  }

  async patch (id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async remove (id: NullableId, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }
}
