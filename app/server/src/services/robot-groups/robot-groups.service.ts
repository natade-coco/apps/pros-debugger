// Initializes the `robot-group` service on path `/robot-group`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { RobotGroups } from './robot-groups.class';
import hooks from './robot-groups.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'robot-groups': RobotGroups & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/robot-groups', new RobotGroups(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('robot-groups');

  service.hooks(hooks);
}
