import { Id, NullableId, Params, ServiceMethods } from '@feathersjs/feathers'
import Axios, { AxiosInstance } from 'axios'

import { Application } from '../../declarations'

interface Data {
  id: string,
  name: string,
  shopName: string
}

const mapRobotGroup = (robotGroup: any): Data => ({
  id: robotGroup.id,
  name: robotGroup.name,
  shopName: robotGroup.shop_name
})

interface ServiceOptions {}

export class RobotGroups implements ServiceMethods<Data> {
  app: Application
  options: ServiceOptions
  axios: AxiosInstance

  constructor (options: ServiceOptions = {}, app: Application) {
    this.options = options
    this.app = app
    this.axios = this.createAxios()
  }

  createAxios = () => {
    const axios = Axios.create({
      baseURL: this.app.get('pros_url') + '/api'
    })
    axios.interceptors.response.use(response => {
      if (response.data.code !== 0) {
        throw new Error(response.data.msg)
      }
      return response
    })
    return axios
  }

  async setup () {}

  async find (params?: Params): Promise<Data[]> {
    const deviceId = params?.query?.device

    try {
      const response = await this.axios.get('/robot/groups', {
        params: { device: deviceId }
      })
      const { data } = response
      const robotGroups: Data[] = data.data.robotGroups.map((robotGroup: any) => mapRobotGroup(robotGroup))
      return robotGroups
    } catch (err) {
      throw err
    }
  }

  async get (id: Id, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async create (data: Data, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async update (id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async patch (id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async remove (id: NullableId, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }
}
