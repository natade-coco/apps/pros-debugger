import { Application } from "../declarations";
import devices from './devices/devices.service';
import robotGroups from './robot-groups/robot-groups.service';
import robots from './robots/robots.service';
import destinations from './destinations/destinations.service';
import status from './status/status.service';
import maps from './maps/maps.service';
import notifications from './notifications/notifications.service';
// Don't remove this comment. It's needed to format import lines nicely.

export default function (app: Application): void {
  app.configure(devices);
  app.configure(robotGroups);
  app.configure(robots);
  app.configure(destinations);
  app.configure(status);
  app.configure(maps);
  app.configure(notifications);
}
