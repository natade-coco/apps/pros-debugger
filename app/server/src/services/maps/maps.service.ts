// Initializes the `maps` service on path `/maps`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Maps } from './maps.class';
import hooks from './maps.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'maps': Maps & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/maps', new Maps(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('maps');

  service.hooks(hooks);
}
