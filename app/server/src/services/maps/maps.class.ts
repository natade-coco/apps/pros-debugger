import { Id, NullableId, Params, ServiceMethods } from '@feathersjs/feathers'
import Axios, { AxiosInstance } from 'axios'

import { Application } from '../../declarations'
import { Data as Destination } from '../destinations/destinations.class'
import { Position } from '../status/status.class'

interface Data extends Destination {
  position: Position
}

const mapMap = (source: any): Data => ({
  id: `${source.id}/${source.mode}`,
  name: source.id,
  type: source.mode,
  position: {
    x: source.vector[0],
    y: source.vector[1],
    angle: source.vector[2]
  }
})

interface ServiceOptions {}

export class Maps implements ServiceMethods<Data> {
  app: Application
  options: ServiceOptions
  axios: AxiosInstance

  constructor (options: ServiceOptions = {}, app: Application) {
    this.options = options
    this.app = app
    this.axios = this.createAxios()
  }

  createAxios = () => {
    const axios = Axios.create({
      baseURL: this.app.get('pros_url') + '/api'
    })
    axios.interceptors.response.use(response => {
      if (response.data.code !== 0) {
        throw new Error(response.data.msg)
      }
      return response
    })
    return axios
  }

  async setup () {}

  async find (params?: Params): Promise<Data[]> {
    const deviceId = params?.query?.device_id
    const robotId = params?.query?.robot_id

    try {
      const response = await this.axios.get('/robot/map', {
        params: {
          device_id: deviceId,
          robot_id: robotId
        }
      })
      const responseData = response.data
      const maps: Data[] = responseData.data.map.elements
        .filter((element: any) => (element.type === 'source'))
        .map((source: any) => mapMap(source))
      return maps
    } catch (err) {
      throw err
    }
  }

  async get (id: Id, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async create (data: Data, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async update (id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async patch (id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async remove (id: NullableId, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }
}
