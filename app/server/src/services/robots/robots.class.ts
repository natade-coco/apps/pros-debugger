import { Id, NullableId, Params, ServiceMethods } from '@feathersjs/feathers'
import Axios, { AxiosInstance } from 'axios'

import { Application } from '../../declarations'

interface Data {
  id: string,
  name: string,
  shopName: string
}

const mapRobot = (robot: any): Data => ({
  id: robot.id,
  name: robot.name,
  shopName: robot.shop_name
})

interface ServiceOptions {}

export class Robots implements ServiceMethods<Data> {
  app: Application
  options: ServiceOptions
  axios: AxiosInstance

  constructor (options: ServiceOptions = {}, app: Application) {
    this.options = options
    this.app = app
    this.axios = this.createAxios()
  }

  createAxios = () => {
    const axios = Axios.create({
      baseURL: this.app.get('pros_url') + '/api'
    })
    axios.interceptors.response.use(response => {
      if (response.data.code !== 0) {
        throw new Error(response.data.msg)
      }
      return response
    })
    return axios
  }

  async setup () {}

  async find (params?: Params): Promise<Data[]> {
    const deviceId = params?.query?.device
    const robotGroupId = params?.query?.group_id

    try {
      const response = await this.axios.get('/robots', {
        params: { device: deviceId, group_id: robotGroupId }
      })
      const { data } = response
      const robots: Data[] = data.data.robots.map((robot: any) => mapRobot(robot))
      return robots
    } catch (err) {
      throw err
    }
  }

  async get (id: Id, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async create (data: Data, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async update (id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async patch (id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async remove (id: NullableId, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }
}
