// Initializes the `robot` service on path `/robot`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Robots } from './robots.class';
import hooks from './robots.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'robots': Robots & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/robots', new Robots(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('robots');

  service.hooks(hooks);
}
