import { Id, NullableId, Params, ServiceMethods } from '@feathersjs/feathers'
import Axios, { AxiosInstance } from 'axios'
import { SDK as Hub } from '@natade-coco/hub-sdk'
import { DIDDocument } from '@natade-coco/hub-sdk/dist/Type'

import { Application } from '../../declarations'
import logger from '../../logger'

interface Data {
  id: string,
  type: string,
  robotId: string,
  robotGroupId: string,
  deviceId: string,
  timestamp: number,
  data: any
}

const mapNotification = (notification: any): Data => ({
  id: notification.msgId,
  type: notification.msgType,
  robotId: notification.robotId,
  robotGroupId: notification.groupId,
  deviceId: notification.deviceId,
  timestamp: notification.timestamp,
  data: notification.data
})

interface ServiceOptions {}

export class Notifications implements ServiceMethods<Data> {
  app: Application
  options: ServiceOptions
  axios: AxiosInstance

  constructor (options: ServiceOptions = {}, app: Application) {
    this.options = options
    this.app = app
    this.axios = this.createAxios()
  }

  createAxios = () => {
    const axios = Axios.create({
      baseURL: this.app.get('pros_url') + '/api'
    })
    axios.interceptors.response.use(response => {
      if (response.data.code !== 0) {
        throw new Error(response.data.msg)
      }
      return response
    })
    return axios
  }

  async setup () {}

  async find (params?: Params): Promise<Data[]> {
    const enable = params?.query?.enable

    if (enable) {
      try {
        const notificationURL = await this._makeNotificationURL()
        await this._postNotificationURL(notificationURL)
      } catch (err) {
        throw err
      }
    }

    return []
  }

  async get (id: Id, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async create (data: any, params?: Params): Promise<Data> {
    logger.info(`[create] Notification received: ${JSON.stringify(data)}`)
    return mapNotification(data);
  }

  async update (id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async patch (id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async remove (id: NullableId, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async _makeNotificationURL (): Promise<string> {
    const appIDKey = '{APP_ID}'
    const deviceIDKey = '{DEVICE_ID}'
    let url: string = this.app.get('notification_url') || `http://${appIDKey}.${appIDKey}-${deviceIDKey}.svc.cluster.local:3030/notifications`
    if (!url.includes(appIDKey) && !url.includes(deviceIDKey)) {
      return url
    }

    try {
      const identity = await this._fetchAppIdentity()

      if (url.includes(appIDKey)) {
        const appID = this._extractServiceID(identity, 'AppSpoke')
        url = url.split(appIDKey).join(appID)
      }

      if (url.includes(deviceIDKey)) {
        const deviceID = this._extractServiceID(identity, 'DeviceSpoke')
        url = url.split(deviceIDKey).join(deviceID)
      }
    } catch (err) {
      throw err
    }

    return url
  }

  async _fetchAppIdentity () {
    const env = this.app.get('node_env')
    const id = this.app.get('natadecoco_id')
    const secret = this.app.get('natadecoco_secret')

    try {
      const client = await Hub.init({
        test: (env !== 'prd'),
        id,
        secret
      })
      return client.Resolve(id)
    } catch (err) {
      throw err
    }
  }

  _extractServiceID (doc: DIDDocument, type: string) {
    const svc = doc.service.filter((svc) => svc.type === type)[0];
    const regex = /[devices|apps]\/(.+)\/spoke/;
    const result = svc.serviceEndpoint.match(regex);
    return result ? result[1] : "";
  }

  async _postNotificationURL (url: string): Promise<void> {
    try {
      await this.axios.post('/notify/url', { url })
      logger.info(`[find] Notification URL<${url}> is successfully registered.`)
    } catch (err) {
      throw err
    }
  }
}
