import { Id, NullableId, Params, ServiceMethods } from '@feathersjs/feathers'
import Axios, { AxiosInstance } from 'axios'

import { Application } from '../../declarations'
import logger from '../../logger'

interface Data {
  id: string,
  name: string,
  region: string,
  secret?: string
}

const mapDevice = (device: any): Data => ({
  id: device.deviceId,
  name: device.name,
  region: device.region
})

interface ServiceOptions {}

export class Devices implements ServiceMethods<Data> {
  app: Application
  options: ServiceOptions
  axios: AxiosInstance

  constructor (options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
    this.axios = this.createAxios()
  }

  createAxios = () => {
    const axios = Axios.create({
      baseURL: this.app.get('pros_url') + '/api'
    })
    axios.interceptors.response.use(response => {
      if (response.data.code !== 0) {
        throw new Error(response.data.msg)
      }
      return response
    })
    return axios
  }

  async setup () {
    const deviceName = this.app.get('device_name')
    const deviceId = this.app.get('device_id')
    const deviceSecret = this.app.get('device_secret')
    const deviceRegion = this.app.get('device_region')
    if (!deviceName || !deviceId || !deviceSecret || !deviceRegion) {
      logger.info('[setup] Device info is not set. Skip add device.')
      return
    }

    try {
      const devices = await this.find()
      if (!devices.some(device => (device.id === deviceId))) {
        await this.create({
          id: deviceId,
          name: deviceName,
          region: deviceRegion,
          secret: deviceSecret
        })
        logger.info(`[setup] Device<${deviceId}> is successfully registered.`)
      } else {
        logger.info(`[setup] Device<${deviceId}> is already registered.`)
      }
    } catch (err) {
      throw err
    }
  }

  async find (params?: Params): Promise<Data[]> {
    try {
      const response = await this.axios.get('/devices')
      const { data } = response
      const devices: Data[] = data.data.devices.map((device: any) => mapDevice(device))
      return devices
    } catch (err) {
      throw err
    }
  }

  async get (id: Id, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async create (data: Data, params?: Params): Promise<Data> {
    try {
      await this.axios.post('/add/device', {
        deviceName: data.name,
        deviceId: data.id,
        deviceSecret: data.secret,
        region: data.region
      })
      return data
    } catch (err) {
      throw err
    }
  }

  async update (id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async patch (id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async remove (id: NullableId, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }
}
