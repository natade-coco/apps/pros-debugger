import { Id, NullableId, Params, ServiceMethods } from '@feathersjs/feathers'
import Axios, { AxiosInstance } from 'axios'

import { Application } from '../../declarations'
import { Data as Destination } from '../destinations/destinations.class'

export interface Position {
  x: number,
  y: number,
  angle: number
}

const mapPosition = (position: any): Position => ({
  x: position.x,
  y: position.y,
  angle: position.angle
})

interface Data {
  chargeStage: string,
  moveState: string,
  robotState: string,
  robotPosition: Position,
  robotPower: number,
  nearestDestination?: Destination
}

const mapStatus = (status: any): Data => ({
  chargeStage: status.chargeStage,
  moveState: status.moveState,
  robotState: status.robotState,
  robotPosition: mapPosition(status.robotPose),
  robotPower: status.robotPower
})

interface ServiceOptions {}

export class Status implements ServiceMethods<Data> {
  app: Application
  options: ServiceOptions
  axios: AxiosInstance

  constructor (options: ServiceOptions = {}, app: Application) {
    this.options = options
    this.app = app
    this.axios = this.createAxios()
  }

  createAxios = () => {
    const axios = Axios.create({
      baseURL: this.app.get('pros_url') + '/api'
    })
    axios.interceptors.response.use(response => {
      if (response.data.code !== 0) {
        throw new Error(response.data.msg)
      }
      return response
    })
    return axios
  }

  async setup () {}

  async find (params?: Params): Promise<Data> {
    const deviceId = params?.query?.device_id
    const robotId = params?.query?.robot_id
    const withNearestDestination = params?.query?.with_nearest_destination

    try {
      const response = await this.axios.get('/robot/status', {
        params: {
          device_id: deviceId,
          robot_id: robotId
        }
      })
      const responseData = response.data
      const status: Data = mapStatus(responseData.data)
      if (withNearestDestination) {
        status.nearestDestination = await this.getNearestDestination(deviceId, robotId, status.robotPosition)
      }
      return status
    } catch (err) {
      throw err
    }
  }

  async get (id: Id, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async create (data: Data, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async update (id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async patch (id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async remove (id: NullableId, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async getNearestDestination (deviceId: string, robotId: string, position: Position): Promise<Destination> {
    try {
      const maps = await this.app.service('maps').find({ query: { device_id: deviceId, robot_id: robotId } })
      let minSS = Infinity
      const nearestMap = maps.reduce((p, c) => {
        if (minSS === Infinity) {
          const pss = calcSumOfSquares(position, p.position)
          minSS = pss
        }
        const css = calcSumOfSquares(position, c.position)
        if (css < minSS) {
          minSS = css
          return c
        } else {
          return p
        }
      })
      return nearestMap
    } catch (err) {
      throw err
    }
  }
}

const calcSumOfSquares = (a: Position, b: Position) => (
  Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2)
)
