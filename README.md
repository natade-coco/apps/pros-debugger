# PROS Debugger

配送ロボット動作確認用のコンテンツアプリ

## 動作条件

同ユニットで[PROSプラグイン](https://gitlab.com/natade-coco/extras/pros)が稼働している必要があります。

## 環境変数

| 変数名 | 説明 | デフォルト |
| :-- | :-- | :-- |
| DEVICE_NAME | デバイス名 | natadeCOCO |
| DEVICE_ID | デバイスID |  |
| DEVICE_SECRET | デバイスシークレット |  |
| DEVICE_REGION | リージョン | ap-northeast-1 |

- デバイスID/シークレットがセットされている場合、サーバーアプリ起動時にPROSに対してデバイスの登録を行います(該当デバイスが未登録の場合のみ)。
- デバイスID/シークレット/リージョンはロボットの管理者から入手してください。
